<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('coming-soon');
});

Route::get('/privacy-policy', function () {
    return view('privacy-policy');
});
Route::get('/terms-of-service', function () {
    return view('terms-of-service');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@index')->name('admin.dashboard');
Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login','Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/artists/getdata','ArtistController@getdata')->name('artists.getdata');
Route::get('/artists/search','ArtistController@search')->name('artists.search');
Route::resource('artists', 'ArtistController');
Route::get('/gigs/getdata','GigController@getdata')->name('gigs.getdata');
Route::resource('gigs', 'GigController');
Route::get('/users/getdata','UserController@getData')->name('users.getdata');
Route::resource('users','UserController');
