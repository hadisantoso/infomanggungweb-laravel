<!DOCTYPE html>
<html class="no-js" prefix="og: http://ogp.me/ns#"  lang="en-IN">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Info Manggung App - Discover Gigs Life</title>
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 

        <meta name="description" content="Info Manggung App - Discover Gigs Life" /> 
        <link href="frontend/img/mobiapp_logo_96dp.png" rel="apple-touch-icon" sizes="96x96">
        <link href="frontend/img/mobiapp_logo_96dp.png" rel="icon" sizes="96x96" type="image/png">
        <link href="frontend/img/mobiapp_logo_32dp.png" rel="icon" sizes="32x32" type="image/png">
        <meta content="frontend/img/mobiapp_logo_96dp.png" name="msapplication-TileImage">
        
        <meta property="og:url" content="https://infomanggung.com/" />
        <meta property="og:title" content="Info Manggung App - Discover Gigs Life" />
        <meta property="og:locale" content="en_IN" /> 
        <meta property="og:site_name" content="Info Manggung" />
        <meta name="twitter:card" content="Info Manggung" />
        <meta name="twitter:site" content="Info Manggung" />
        <meta name="twitter:creator" content="SCL - Sidechain Labs" />

        <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
        <link rel='stylesheet' href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="frontend/device-mockups/device-mockups.min.css">
        <link rel="stylesheet" href="frontend/css/owl.carousel.min.css">
        <link rel="stylesheet" href="frontend/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="frontend/css/animate.min.css">
        <link rel="stylesheet" href="frontend/css/app.css">

        <script src="frontend/js/jquery.min.js"></script>
        <script> $(window).on('load', function(){ $(".loader").fadeOut(2000); }); </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57854559-3"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-57854559-3');
        </script>

    </head>

    <body>

        <div class="loader"></div>
        <!-- NAVIGATION 
            ==============-->
            <!-- <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
              <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="index.html#home"><img src="frontend/img/logo.png" alt="" class="img-fluid"></a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                  Menu
                  <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                  <ul class="navbar-nav text-uppercase ml-auto">
                    <li class="nav-item">
                      <a class="nav-link js-scroll-trigger" href="#contact">Contact us</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link js-scroll-trigger" href="#download">Download Now </a>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>   -->

        <!--APP HEADER
            ================--> 
            <section class="masthead">
              <div class="container h-100">
                <div class="row h-100">
                  <div class="col-lg-7 my-auto text-center">
                    <div class="header-content mx-auto  animated wow zoomIn" data-wow-duration="0.3s" data-wow-delay=".1s">
                      <img src="frontend/img/logo.png" alt="" class="img-fluid mobile">
                      <h1 class="mx-auto">Info Manggung App</h1>
                      <h3 class="mx-auto">Guidance for Discover Gigs Life</h3>
                      <a href="https://play.google.com/store/apps/details?id=com.sidechainlabs.infomanggung" target="_blank"><img src="frontend/img/app/android.png" class="android-download" alt=""></a>
                      <!-- <a href="#download"><button  class="btn btn-white mt-4 py-2 px-3 ">Download Now <i class="fas fa-arrow-alt-circle-down"></i></button></a> -->
                    </div>
                  </div>
                  <div class="col-lg-5 my-auto">
                    <div class="device-container">
                      <div class="device-mockup iphone6_plus portrait white animated wow bounceInUp" data-wow-duration="1s" data-wow-delay=".5s">
                        <div class="device">
                          <div class="screen">
                            <img src="frontend/img/app/six.png" class="img-fluid" alt="">
                          </div>
                          <div class="button">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <div class="bg-banner-img">
              <!-- <img src="img/bg-curve.png" alt=""> -->
            </div>

        <!-- FOOTER 
            ==============-->
            <footer id="footer" class="footer">
              <div class="container-fluid">
                <div class="row top-footer">
                  <div class="col-md-12 text-center">
                    <p>Copyrights &copy; 2018 | <a href="https://sidechainlabs.com/">SCL - Sidechain Labs</a></p>
                  </div> 
                </div> 
              </div>
            </footer>

        <script src="frontend/js/popper.min.js"></script>
        <script src="frontend/js/bootstrap.min.js"></script>
        <script src="frontend/js/jquery.easing.min.js"></script>
        <script src="frontend/js/owl.carousel.min.js"></script>
        <script src="frontend/js/wow.min.js"></script>        
        <script src="frontend/js/app.js"></script>
        
    </body>
</html>