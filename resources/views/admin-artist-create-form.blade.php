@extends('layouts.admin-flip')
@section('content')
<style>
  img {
    max-width: 100%; /* This rule is very important, please do not ignore this! */
  }
</style>
<div class="col-md-6">
      <div class="box">
        <div class="box-header">
          <h2>Add Artist</h2>
        </div>
        <div class="box-divider m-0"></div>
        <div class="box-body">
          <form role="form" method="post" action="{{ route('artists.store') }}" id="form" enctype="multipart/form-data"  >
            {{ csrf_field() }}
            <div class="form-group row">
              <label for="name" class="col-sm-2 form-control-label">Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name='name' placeholder="Name">
              </div>
            </div>
            <div class="form-group row">
              <label for="bioindo" class="col-sm-2 form-control-label">Bio. Indo</label>
              <div class="col-sm-10">
                <textarea class="form-control" rows="2" name='bioindo' placeholder="Say what you say ..."></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label for="bioenglish" class="col-sm-2 form-control-label">Bio. English</label>
              <div class="col-sm-10">
                <textarea class="form-control" rows="2" name='bioenglish' placeholder="Say what you say ..."></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label for="photo_profile" class="col-sm-2 form-control-label">Photo Profile</label>
              <div class="col-sm-10">
                <input type="button" name="btn" value="Upload" id="uploadPflBtn" class="btn" />
              </br>
                <img id="photoProfile" width="150">
                <input type="hidden"  id="photoProfileImg" name="photo_profile">
              </div>
            </div>
            <div class="form-group row">
              <label for="photo_cover" class="col-sm-2 form-control-label">Photo Cover</label>
              <div class="col-sm-10">
                <input type="button" name="btn" value="Upload" id="uploadCvrBtn" class="btn" />
              </br>
                <img id="photoCover" width="150">
                <input type="hidden"  id="photoCoverImg" name="photo_cover">
              </div>
            </div>
            <div class="form-group row">
              <label for="email" class="col-sm-2 form-control-label">Email</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name='email' placeholder="Email">
              </div>
            </div>
            <div class="form-group row">
              <label for="phone" class="col-sm-2 form-control-label">Phone</label>
              <div class="col-sm-10">
                <input type="number" class="form-control" name='phone' placeholder="Phone">
              </div>
            </div>
            <div class="form-group row">
              <label for="instagram" class="col-sm-2 form-control-label">Instagram</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name='instagram' placeholder="Instagram">
              </div>
            </div>
            <div class="form-group row">
              <label for="twitter" class="col-sm-2 form-control-label">Twitter</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name='twitter' placeholder="Twitter">
              </div>
            </div>
            <div class="form-group row">
              <label for="facebook" class="col-sm-2 form-control-label">Facebook</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name='facebook' placeholder="Facebook">
              </div>
            </div>
            <div class="form-group row">
              <label for="soundcloud" class="col-sm-2 form-control-label">Soundcloud</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name='soundcloud' placeholder="Soundcloud">
              </div>
            </div>
            <div class="form-group row">
              <label for="bandcamp" class="col-sm-2 form-control-label">Bandcamp</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name='bandcamp' placeholder="Bandcamp">
              </div>
            </div>
            <div class="form-group row">
              <label for="reverbnation" class="col-sm-2 form-control-label">Reverbnation</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name='reverbnation' placeholder="Reverbnation">
              </div>
            </div>
            <div class="form-group row">
              <label for="youtube" class="col-sm-2 form-control-label">Youtube</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name='youtube' placeholder="Youtube">
              </div>
            </div>
            <div class="form-group row">
              <label for="spotify" class="col-sm-2 form-control-label">Spotify</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name='spotify' placeholder="Spotify">
              </div>
            </div>
            <div class="form-group row">
              <label for="website" class="col-sm-2 form-control-label">Website</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name='website' placeholder="Website">
              </div>
            </div>
            <div class="form-group row">
              <label for="artists" class="col-sm-2 form-control-label">Publish</label>
              <div class="col-sm-10">
                <div class="form-control">
                <label class="ui-check col-sm-2">
                  <input type="checkbox" id="publish" name="is_published"><i></i> YES
                </label>
                </div>
              </div>
            </div>
            <div class="form-group row m-t-md">
              <div class="col-sm-offset-2 col-sm-12">
                <input type="button" name="btn" value="Submit" id="submitBtn" data-toggle="modal" data-target="#confirm-submit" class="btn btn-primary pull-right" />
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
@include('layouts.modal-submit')
@include('layouts.modal-image-upload')
@push('scripts')
  <script>
  $('#uploadPflBtn').click(function(){
    if(cropper!=""){
      cropper.destroy();
    }
    cropper = new Cropper(image, {
      aspectRatio: 1 / 1,
    });
    idPhoto = "photoProfile";
    idPhotoData = "photoProfileImg";
    $('#upload-image').modal('toggle');
  });
  $('#uploadCvrBtn').click(function(){
    if(cropper!=""){
      cropper.destroy();
    }
    cropper = new Cropper(image, {
      aspectRatio: 4 / 3,
    });
    idPhoto = "photoCover";
    idPhotoData = "photoCoverImg";
    $('#upload-image').modal('toggle');
  });
  </script>
@endpush
@endsection
