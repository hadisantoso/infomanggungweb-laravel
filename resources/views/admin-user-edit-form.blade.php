@extends('layouts.admin-flip')
@section('content')
<style>
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }
      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

    #pac-input:focus {
      border-color: #4d90fe;
    }

    .pac-container {
      font-family: Roboto;
    }
    img {
      max-width: 100%; /* This rule is very important, please do not ignore this! */
    }
</style>
<div class="col-md-6">
      <div class="box">
        <div class="box-header">
          <h2>Edit User</h2>
        </div>
        <div class="box-divider m-0"></div>
        <div class="box-body">
          <form role="form" method="post" action="{{ route('users.update',$user->id) }}" id="form" enctype="multipart/form-data"  >
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="put" />
            <div class="form-group row">
              <label for="name" class="col-sm-2 form-control-label">Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name='name' placeholder="Name" value="{{$user->name}}">
              </div>
            </div>
            <div class="form-group row">
              <label for="description" class="col-sm-2 form-control-label">Email</label>
              <div class="col-sm-10">
                  <input type="text" readonly class="form-control" name='email' placeholder="Email" value="{{$user->email}}">
              </div>
            </div>
            <div class="form-group row m-t-md">
              <div class="col-sm-offset-2 col-sm-12">
                <input type="button" name="btn" value="Submit" id="submitBtn" data-toggle="modal" data-target="#confirm-submit" class="btn btn-primary pull-right" />
                <input type="button" name="btn" value="Delete" id="deleteBtn" data-toggle="modal" data-target="#confirm-delete" class="btn btn-warning" />
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!--bootstrap modal-->
    <div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  Confirm Submit
              </div>
              <div class="modal-body">
                  Are you sure to submit ?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="#" id="submit" class="btn btn-success success">Submit</a>
              </div>
          </div>
      </div>
    </div>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  Confirm Submit
              </div>
              <div class="modal-body">
                  Are you sure to delete ?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="#" data-method="delete" id="delete" class="btn btn-success success">Submit</a>
              </div>
          </div>
      </div>
    </div>
    <!--end bootstrap modal-->

@include('layouts.modal-submit')
@push('scripts')
  <script type="text/javascript">

  $('#delete').click(function(){
    alert('test');
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    var $this = $(this);
    $.ajax({
        type: 'delete',
        url: '{{ route('users.destroy',$user->id) }}'
    }).done(function (data) {
      window.location = '{{ route('users.index') }}';
    });
  });
  </script>
@endpush
@endsection
