@extends('layouts.admin-flip')
@section('content')
<!-- ############ PAGE START-->
<div class="padding">
  <div class="box">
    <div class="box-header">
      <h2>Project List</h2>
    </div>
    <div class="table-responsive">
      <table ui-jp="dataTable" id="users-table"
          ui-options="{
              sAjaxSource: '{!! route('gigs.getdata') !!}',
              aoColumns: [
                { mData: 'id' },
                { mData: 'name' },
                { mData: 'place' },
                { mData: 'event_date' },
                { mData: 'is_gigs_slider'},
                { mData: 'is_published'},
                {
				           mData: 'id',
				           mRender: function(data) {
                     var url = '<a class=btn  href={{ route('gigs.edit', ':id') }}>Details</a>';
                     url = url.replace(':id',data);
                     return url;
				         }
			          }
              ]}"
          class="table table-striped b-t b-b">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Place</th>
            <th>Event Date</th>
            <th>Gigs Slider</th>
            <th>Publish</th>
            <th>Details</th>
          </tr>
        </thead>

      </table>
    </div>
  </div>
</div>

@endsection
