@extends('layouts.admin-flip')
@section('content')
<style>
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }
      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

    #pac-input:focus {
      border-color: #4d90fe;
    }

    .pac-container {
      font-family: Roboto;
    }
    img {
      max-width: 100%; /* This rule is very important, please do not ignore this! */
    }
</style>
<div class="col-md-6">
      <div class="box">
        <div class="box-header">
          <h2>Add Gig</h2>
        </div>
        <div class="box-divider m-0"></div>
        <div class="box-body">
          <form role="form" method="post" action="{{ route('gigs.update',$gig->id) }}" id="form" enctype="multipart/form-data"  >
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="put" />
            <div class="form-group row">
              <label for="name" class="col-sm-2 form-control-label">Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name='name' placeholder="Name" value="{{$gig->name}}">
              </div>
            </div>
            <div class="form-group row">
              <label for="description" class="col-sm-2 form-control-label">Description</label>
              <div class="col-sm-10">
                <textarea class="form-control" rows="2" name='description' placeholder="Say what you say ...">{{$gig->description}}</textarea>
              </div>
            </div>
            <div class="form-group row">
              <label for="photo_cover" class="col-sm-2 form-control-label">Photo Cover</label>
              <div class="col-sm-10">
                <input type="button" name="btn" value="Upload" id="uploadCvrBtn" class="btn" />
              </br>
                <img id="photoCover" src="{{config('constants.aws_prefix')}}{{ $gig->photo_cover}}" width="150">
                <input type="hidden"  id="photoCoverImg" name="photo_cover">
              </div>
            </div>

            <div class="form-group row">
              <label for="name" class="col-sm-2 form-control-label">Event Date</label>
              <div class='col-sm-10 input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" name='event_date' value="{{$gig->event_date}}"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group row">
              <label for="city" class="col-sm-2 form-control-label">City</label>
              <div class="col-sm-10">
                <input type="text" id="autocomplete" class="form-control" placeholder="City" value="{{$gig->location->city_name}}">
                <input type="hidden" name="google_place_id">
                <input type="hidden" name="city_name">
              </div>
            </div>
            <div class="form-group row">
              <label for="place" class="col-sm-2 form-control-label">Place</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name='place' placeholder="Place" value="{{$gig->place}}">
              </div>
            </div>
            <div class="form-group row">
              <label for="location" class="col-sm-2 form-control-label">Place In Map</label>
              <div class="col-sm-10">
                <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                <div id="map" style="width:100%;height:400px;"></div>
                <input type="hidden" name="latitude">
                <input type="hidden" name="longitude">
              </div
            <div class="form-group row">
              <label for="artists" class="col-sm-2 form-control-label">Artists</label>
              <div class="col-sm-10">
                <textarea class="form-control test" rows="2" name='artists' placeholder="@artists" id="txtarea_artists"></textarea>
                <input type="hidden" name="artists_uid">
                <input type="hidden" name="artists_mention_code">
              </div>
            </div>
            <div class="form-group row">
              <label for="artists" class="col-sm-2 form-control-label">Gigs Slider</label>
              <div class="col-sm-10">
                <div class="form-control">
                <label class="ui-check col-sm-2">
                  <input type="checkbox" id="isGigsSlider" name="is_gigs_slider" {{($gig->is_gigs_slider==1) ? 'checked':''}}><i></i> YES
                </label>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="artists" class="col-sm-2 form-control-label">Publish</label>
              <div class="col-sm-10">
                <div class="form-control">
                <label class="ui-check col-sm-2">
                  <input type="checkbox" id="publish" name="is_published" {{($gig->is_published==1) ? 'checked':''}}><i></i> YES
                </label>
                </div>
              </div>
            </div>
            <div class="form-group row m-t-md">
              <div class="col-sm-offset-2 col-sm-12">
                <input type="button" name="btn" value="Submit" id="submitBtn" data-toggle="modal" data-target="#confirm-submit" class="btn btn-primary pull-right" />
                <input type="button" name="btn" value="Delete" id="deleteBtn" data-toggle="modal" data-target="#confirm-delete" class="btn btn-warning" />
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!--bootstrap modal-->
    <div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  Confirm Submit
              </div>
              <div class="modal-body">
                  Are you sure to submit ?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="#" id="submit" class="btn btn-success success">Submit</a>
              </div>
          </div>
      </div>
    </div>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  Confirm Submit
              </div>
              <div class="modal-body">
                  Are you sure to delete ?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="#" data-method="delete" id="delete" class="btn btn-success success">Submit</a>
              </div>
          </div>
      </div>
    </div>
    <!--end bootstrap modal-->

@include('layouts.modal-submit')
@include('layouts.modal-image-upload')
@push('scripts')
  <script type="text/javascript">

            $(function () {
                $('#datetimepicker1').datetimepicker();
                $('#txtarea_artists').mentionsInput({
                  source: '{{ route('artists.search') }}',
                  suffix: ', ',
                  autocomplete : {
                    minLength: 2,
                    delay: 0
                  }
                });
                $('#txtarea_artists').mentionsInput('setValue','{{$gig->artists_mention_code}}'); //set default value
            });

            $('#uploadCvrBtn').click(function(){
                if(cropper!=""){
                  cropper.destroy();
                }
                cropper = new Cropper(image, {
                  aspectRatio: 4 / 3,
                });
                idPhoto = "photoCover";
                idPhotoData = "photoCoverImg";
                $('#upload-image').modal('toggle');
              });
            $('#submit').click(function(){
               var artists_mention_code = $('#txtarea_artists').mentionsInput('getValue'); //get value dengan code mention
               var artistsuid = $('#txtarea_artists').mentionsInput('getMentions'); //get map string dengan uid
               var artistsraw = $('#txtarea_artists').mentionsInput('getRawValue'); //get value biasa sesuai text
               //console.log("test : "+artistsraw);
               var uid = artistsuid.map(function(artist) {
                   return artist['uid'];
               });
                $('input[name=artists]').val(artistsraw);
                $('input[name=artists_uid]').val(uid);
                $('input[name=artists_mention_code]').val(artists_mention_code);
                $('#form').submit();
              });

            //delete
            $('#delete').click(function(){
              $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });

              var $this = $(this);
              $.ajax({
                  type: 'delete',
                  url: '{{ route('gigs.destroy',$gig->id) }}'
              }).done(function (data) {
                window.location = '{{ route('gigs.index') }}';
              });
            });

            function myMap() {
              $('input[name=latitude]').val({{$gig->latitude}});
              $('input[name=longitude]').val({{$gig->longitude}});
              var map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: -2.44565, lng: 117.8888},
                    zoom: 4,
                    mapTypeId: 'roadmap'
               });
               var input = document.getElementById('pac-input');
               var searchBox = new google.maps.places.SearchBox(input);
               map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

               map.addListener('bounds_changed', function() {
                 searchBox.setBounds(map.getBounds());
               });

               var markers = [];
               var latitude = "{{$gig->latitude}}";
               var longitude = "{{$gig->longitude}}";
               if(latitude!="" && longitude!=""){
                 var defaultLoc = new google.maps.LatLng(latitude, longitude);
                 map.setZoom(14);
                 markers.push(new google.maps.Marker({
                   map: map,
                   draggable: true,
                   position: defaultLoc
                 }));
                 map.setCenter(defaultLoc);
                 google.maps.event.addListener(markers[0], 'dragend', function(evt){
                   $('input[name=latitude]').val(evt.latLng.lat());
                   $('input[name=longitude]').val(evt.latLng.lng());
                 });
               }
               searchBox.addListener('places_changed', function() {
                 var places = searchBox.getPlaces();

                 if (places.length == 0 || places.length > 1) {
                   return;
                 }
                 markers.forEach(function(marker) {
                   marker.setMap(null);
                 });
                 markers = [];
                 var bounds = new google.maps.LatLngBounds();
                 places.forEach(function(place) {
                    if (!place.geometry) {
                     return;
                    }
                   // Create a marker for each place.
                   markers.push(new google.maps.Marker({
                     map: map,
                     title: place.name,
                     draggable: true,
                     position: place.geometry.location
                   }));
                   $('input[name=latitude]').val(place.geometry.location.lat());
                   $('input[name=longitude]').val(place.geometry.location.lng());
                   if (place.geometry.viewport) {
                     // Only geocodes have viewport.
                     bounds.union(place.geometry.viewport);
                   } else {
                     bounds.extend(place.geometry.location);
                   }

                   google.maps.event.addListener(markers[0], 'dragend', function(evt){
                     $('input[name=latitude]').val(evt.latLng.lat());
                     $('input[name=longitude]').val(evt.latLng.lng());
                   });
                 });
                 map.fitBounds(bounds);
               });
               initAutoComplete();
            }

            var autocomplete;
            function initAutoComplete() {
               autocomplete = new google.maps.places.Autocomplete(
                   /** @type {HTMLInputElement} */
                   (document.getElementById('autocomplete')),
                   { types: ['(cities)'] });
               google.maps.event.addListener(autocomplete, 'place_changed', function() {
                 $('input[name=google_place_id]').val(autocomplete.getPlace().place_id);
                 $('input[name=city_name]').val(autocomplete.getPlace().name);
               });
             }

            //  Penjagaan untuk multiple request API
             if (typeof google === 'object' && typeof google.maps === 'object') {
               myMap();
             }else{
               $.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyCJ_1mqqBCEsOOheo28I-5EAv_CqvAJ4cg&callback=myMap&libraries=places');
             }

  </script>

@endpush
@endsection
