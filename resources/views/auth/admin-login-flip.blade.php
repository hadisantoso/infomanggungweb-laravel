@extends('layouts.admin-login-flip')
@section('content')
<body style="background: url('../assets/images/bg.png') center top no-repeat; background-size: cover;">
  <div class="app" id="app">

<!-- ############ LAYOUT START-->
  <br><br><br><br>
  <div class="center-block w-xxl w-auto-xs p-y-md">
    <div class="navbar">
      <div class="pull-center">
        <!-- <a class="navbar-brand"> 
          <div ui-include="'../assets/images/logo.svg'"></div> 
          <img src="../assets/images/logo-infomanggung.png" alt="." >
          <span class="m-b text-lg">Info Manggung</span>
        </a>-->
      </div>
    </div>
    <div class="p-a-lg box-color r box-shadow-z1 text-color m-a">
      <div class="m-b text-md text-center">
        <img src="../assets/images/logo-infomanggung.png" alt="." style="max-height: 80px;">
      </div>
      <div class="m-b text-md text-center">
        <strong>INFO MANGGUNG</strong>
      </div>
      <form name="form" method="post" action="{{ route('admin.login.submit') }}">
        {{ csrf_field() }}
        <div class="md-form-group float-label">
          <input type="email" class="md-input" id="email" name="email" value="{{ old('email') }}" required autofocus >
          <label>Email</label>
        </div>
        <div class="md-form-group float-label">
          <input type="password" class="md-input" id="password" name="password" required>
          <label>Password</label>
        </div>
        <div class="m-b-md">
          <label class="md-check">
            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}><i class="primary"></i> Keep me signed in
          </label>
        </div>
        <button type="submit" class="btn primary btn-block p-x-md">Sign in</button>
      </form>
    </div>

  </div>

<!-- ############ LAYOUT END-->

  </div>
</body>
@endsection
