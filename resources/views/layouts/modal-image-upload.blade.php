<div class="modal fade" id="upload-image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              Upload Image
          </div>
          <div class="modal-body">
            <input type="file" id="upload">
            <br/>
            <div style="widht:">
              <img id="image">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <a href="#" id="submitImage" class="btn btn-success success">Submit</a>
          </div>
	  	</div>
  </div>
</div>
<script>
var idPhoto = "";
var idPhotoData = "";
var image = document.getElementById('image');
var cropper = "";
var reader = new FileReader();
$('#upload').on('change', function () {
    reader.onload = function (e) {
    	cropper.replace(e.target.result);
    }
    reader.readAsDataURL(this.files[0]);
    $('#upload').val("");
});
$('#submitImage').on('click', function () {
    var data = cropper.getCroppedCanvas({
      maxWidth : 2048,
      maxHeight : 2048
    }).toDataURL();
    $('#'+idPhoto).attr('src',data);
    $('#upload-image').modal('toggle');
    $('#'+idPhotoData).val(data);
    $('#upload').val("");
});

</script>
