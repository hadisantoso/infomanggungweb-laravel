<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- for ios 7 style, multi-resolution icon of 152x152 -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="../assets/images/logo.png">
    <meta name="apple-mobile-web-app-title" content="Flatkit">
    <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" sizes="196x196" href="../assets/images/logo.png">

    <!-- style -->
    <link rel="stylesheet" href={{ URL::asset('assets/animate.css/animate.min.css') }} type="text/css" />
    <link rel="stylesheet" href={{ URL::asset('assets/glyphicons/glyphicons.css') }} type="text/css" />
    <link rel="stylesheet" href={{ URL::asset('assets/font-awesome/css/font-awesome.min.css') }} type="text/css" />
    <link rel="stylesheet" href={{ URL::asset('assets/material-design-icons/material-design-icons.css') }} type="text/css" />
    <link rel="stylesheet" href={{ URL::asset('assets/bootstrap/dist/css/bootstrap.min.css') }} type="text/css" />
    <link rel="stylesheet" href={{ URL::asset('assets/bootstrap/dist/css/bootstrap-datetimepicker.min.css') }} type="text/css" />

    <!-- build:css ../assets/styles/app.min.css -->
    <link rel="stylesheet" href={{ URL::asset('assets/styles/app.css') }} type="text/css" />
    <!-- endbuild -->
    <link rel="stylesheet" href={{ URL::asset('assets/styles/font.css') }} type="text/css" />

    <!-- jQuery -->
    <link rel="stylesheet" href={{ URL::asset('assets/css/jquery.mentions.css') }} type="text/css" />
    <link rel="stylesheet" href={{ URL::asset('assets/css/jquery-ui.css') }} type="text/css" />
    <script src={{ URL::asset('libs/jquery/jquery/dist/jquery.js') }} ></script>
    <script src={{ URL::asset('libs/jquery/jquery/dist/jquery-ui.js') }} ></script>
    <script src={{ URL::asset('libs/jquery/jquery-mention/jquery.mentions.js') }} ></script>


    <script src={{ URL::asset('libs/js/cropper/cropper.js') }}></script>
    <link rel="stylesheet" href={{ URL::asset('assets/cropper/cropper.css') }} type="text/css" />




</head>
<body>
  <div class="app" id="app">

<!-- ############ LAYOUT START-->

  <!-- aside -->
  <div id="aside" class="app-aside modal nav-dropdown">
  	<!-- fluid app aside -->
    <div class="left navside dark dk" data-layout="column">
  	  <div class="navbar no-radius">
        <!-- brand -->
        <a class="navbar-brand">
        	<!-- <div ui-include="'../assets/images/logo.svg'"></div> -->
        	<img src={{ URL::asset('assets/images/logo-infomanggung.png')}} alt="." >
        	<span class="hidden-folded inline">Info Manggung</span>
        </a>
        <!-- / brand -->
      </div>
      <div class="hide-scroll" data-flex>
          <nav class="scroll nav-light">

              <ul class="nav" ui-nav>
                <li class="nav-header hidden-folded">
                  <small class="text-muted"><hr></small>
                </li>
                <!-- ############ ARTIST MENU ############-->
                <li>
                  <a>
                    <span class="nav-caret">
                      <i class="fa fa-caret-down"></i>
                    </span>
                    <span class="nav-icon">
                      <i class="material-icons">&#xe5c3;
                        <span ui-include="'{{ URL::asset('../assets/images/i_1.svg')}}'"></span>
                      </i>
                    </span>
                    <span class="nav-text">Artists</span>
                  </a>
                  <ul class="nav-sub">
                    <li>
                      <a href="{{ route('artists.create') }}" >
                        <span class="nav-text">Add New Artist</span>
                      </a>
                    </li>
                    <li>
                      <a href="{{ route('artists.index') }}" >
                        <span class="nav-text">Artist List</span>
                      </a>
                    </li>
                  </ul>
                </li>

                <!-- ############ GIG MENU ############-->
                <li>
                  <a>
                    <span class="nav-caret">
                      <i class="fa fa-caret-down"></i>
                    </span>
                    <span class="nav-icon">
                      <i class="material-icons">&#xe5c3;
                        <span ui-include="'{{ URL::asset('../assets/images/i_1.svg')}}'"></span>
                      </i>
                    </span>
                    <span class="nav-text">Gigs</span>
                  </a>
                  <ul class="nav-sub">
                    <li>
                      <a href="{{ route('gigs.create') }}" >
                        <span class="nav-text">Add New Gig</span>
                      </a>
                    </li>
                    <li>
                      <a href="{{ route('gigs.index') }}" >
                        <span class="nav-text">Gig List</span>
                      </a>
                    </li>
                  </ul>
                </li>

                <!-- ############ USERS MENU ############-->
                <li>
                  <a>
                    <span class="nav-caret">
                      <i class="fa fa-caret-down"></i>
                    </span>
                    <span class="nav-icon">
                      <i class="material-icons">&#xe5c3;
                        <span ui-include="'{{ URL::asset('../assets/images/i_1.svg')}}'"></span>
                      </i>
                    </span>
                    <span class="nav-text">Users</span>
                  </a>
                  <ul class="nav-sub">
                    <li>
                      <a href="{{ route('users.index') }}" >
                        <span class="nav-text">User List</span>
                      </a>
                    </li>
                  </ul>
                </li>

                <li class="nav-header hidden-folded">
                  <small class="text-muted"><hr></small>
                </li>

              </ul>
          </nav>
      </div>
    </div>
  </div>
  <!-- / -->
  <!-- content -->
  <div id="content" class="app-content box-shadow-z0" role="main">
    <div class="app-header white box-shadow">
        <div class="navbar navbar-toggleable-sm flex-row align-items-center">
            <!-- Open side - Naviation on mobile -->
            <a data-toggle="modal" data-target="#aside" class="hidden-lg-up mr-3">
              <i class="material-icons">&#xe5d2;</i>
            </a>
            <!-- / -->

            <!-- Page title - Bind to $state's title -->
            <div class="mb-0 h5 no-wrap" ng-bind="$state.current.data.title" id="pageTitle"></div>

            <!-- navbar collapse -->
            <div class="collapse navbar-collapse" id="collapse">
              <!-- link and dropdown -->
              <ul class="nav navbar-nav mr-auto">
                <li class="nav-item dropdown">
                  <a class="nav-link" href data-toggle="dropdown">
                    <!-- <i class="fa fa-fw fa-plus text-muted"></i> -->
                    <span>DASHBOARD</span>
                  </a>
                </li>
              </ul>

              <!-- / -->
            </div>
            <!-- / navbar collapse -->

            <!-- navbar right -->
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
            <!-- / navbar right -->
        </div>
    </div>
    <div class="app-footer">
      <div class="p-2 text-xs">
        <div class="pull-right text-muted py-1">
           PT. Hutchison <strong>3</strong> Indonesia &copy; 2017 <span class="hidden-xs-down"></span>
          <a ui-scroll-to="content"><i class="fa fa-long-arrow-up p-x-sm"></i></a>
        </div>
        <div class="nav">
          <a class="nav-link" href="../">About</a>
        </div>
      </div>
    </div>
    <div ui-view class="app-body" id="view">

@yield('content')
<!--SCRIPT-->
@stack('scripts')
@include('alert::bootstrap')
</body>
<!-- build:js scripts/app.html.js -->

<!-- Bootstrap -->
  <script src={{ URL::asset('libs/jquery/tether/dist/js/tether.min.js') }}></script>
  <script src={{ URL::asset('libs/jquery/bootstrap/dist/js/bootstrap.js') }}></script>

  <script src={{ URL::asset('libs/js/moment/moment.js')}}></script>
  {{-- https://github.com/AuspeXeu/bootstrap-datetimepicker --}}
  <script src={{ URL::asset('libs/jquery/bootstrap/dist/js/bootstrap-datetimepicker.min.js')}}></script>

<!-- core -->
  <script src={{ URL::asset('libs/jquery/underscore/underscore-min.js') }}></script>
  <script src={{ URL::asset('libs/jquery/jQuery-Storage-API/jquery.storageapi.min.js') }}></script>
  <script src={{ URL::asset('libs/jquery/PACE/pace.min.js') }}></script>

  <script src={{ URL::asset('libs/jquery/datatables/media/js/jquery.dataTables.min.js') }} ></script>

  <script src={{ URL::asset('scripts/config.lazyload.js') }}></script>

  <script src={{ URL::asset('scripts/palette.js') }}></script>
  <script src={{ URL::asset('scripts/ui-load.js') }}></script>
  <script src={{ URL::asset('scripts/ui-jp.js') }}></script>
  <script src={{ URL::asset('scripts/ui-include.js') }}></script>
  <script src={{ URL::asset('scripts/ui-device.js') }}></script>
  <script src={{ URL::asset('scripts/ui-form.js') }}></script>
  <script src={{ URL::asset('scripts/ui-nav.js') }}></script>
  <script src={{ URL::asset('scripts/ui-screenfull.js') }}></script>
  <script src={{ URL::asset('scripts/ui-scroll-to.js') }}></script>
  <script src={{ URL::asset('scripts/ui-toggle-class.js') }}></script>

  <script src={{ URL::asset('scripts/app.js') }}></script>

  <!-- ajax -->
  <script src={{ URL::asset('libs/jquery/jquery-pjax/jquery.pjax.js') }}></script>
  <script src={{ URL::asset('scripts/ajax.js') }}></script>


<!-- endbuild -->

</html>
