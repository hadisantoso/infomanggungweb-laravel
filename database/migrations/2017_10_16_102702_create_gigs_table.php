<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gigs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->longText('description')->nullable();
            $table->string('photo_cover')->nullable();
            $table->string('photo_cover_thumbnail')->nullable();
            $table->dateTime('event_date')->nullable();
            $table->bigInteger('location_id')->unsigned()->nullable();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->string('place')->nullable();
            $table->decimal('latitude',17,15)->nullable();
            $table->decimal('longitude',18,15)->nullable();
            $table->longText('artists')->nullable();
            $table->longText('artists_mention_code')->nullable();
            $table->string('is_gigs_slider',1)->nullable();
            $table->string('is_published',1)->nullable();
            $table->bigInteger('click_count')->unsigned()->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gigs');
    }
}
