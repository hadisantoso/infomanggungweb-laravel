<?php

namespace App\GeneralClasses;

use Illuminate\Support\Facades\Log;

/**
 *
 */
class Parser
{
    /**
     * Get file name from databases images
     * @param String $data
     * @return String
     */
    public static function getFileName($data)
    {
        $data = explode('/', $data);
        $count = sizeof($data) - 1;
        return $data[$count];
    }


    /**
     * Get array from parsing string
     * @param String $data
     * @param String $string
     * @return ArrayObject $data
     */
    public static function getParserString($data, $parse)
    {
        if (empty($data)) {
            return null;
        }
        $data = explode($parse, $data);
        return $data;
    }
}
