<?php

namespace App\GeneralClasses;

use Image;
use Storage;
use Illuminate\Support\Facades\Log;

/**
 *
 */
class S3Image
{

    /**
     * Use for save image to s3 amazon ws and return location path
     * Full imaage and Thumbnail
     * @param Image $file
     * @param String $size
     * @param String $old_photo
     * @return String $data
     *
     */
    public static function save($file, $size, $old_photo)
    {
        $image = Image::make($file);
        if ($size == 'profile') {
            $image->resize(400, 400);
        } elseif ($size == 'cover') {
            $image->resize(1080, 810);
        }

        $image_thumbnail = Image::make($file);
        if ($size == 'profile') {
            $image_thumbnail->resize(30, 30);
        } elseif ($size == 'cover') {
            $image_thumbnail->resize(60, 45);
        }

        $image->encode('jpg');
        $image_thumbnail->encode('jpg');
        $id_pict = 'images/' . date('Y-m-d') . '/' . uniqid('img_');
        if (!empty($old_photo)) {
            Log::info('Deleting old Photo : ' . $old_photo->image);
            Storage::disk('s3')->delete($old_photo->image);
            Log::info('Deleting old Photo : ' . $old_photo->thumbnail);
            Storage::disk('s3')->delete($old_photo->thumbnail);
        }
        Storage::disk('s3')->put($id_pict . '.jpg', $image->__toString(), 'public');
        Storage::disk('s3')->put($id_pict . '_thumbnail.jpg', $image_thumbnail->__toString(), 'public');
        $result = new \stdClass();;
        $result->image = $id_pict . '.jpg';
        $result->thumbnail = $id_pict . '_thumbnail.jpg';
        return $result;
    }

    public static function delete($old_photo)
    {
        if (!empty($old_photo)) {
            Log::info('Deleting old Photo : ' . $old_photo);
            Storage::disk('s3')->delete('images/' . $old_photo);
        }
    }
}
