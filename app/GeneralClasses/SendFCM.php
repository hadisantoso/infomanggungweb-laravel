<?php

namespace App\GeneralClasses;

use App\FirebaseId;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use Illuminate\Support\Facades\Log;

class SendFcm
{
    public static function send($tokens, $body, $gig_id, $gig_name)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder("Info Manggung");
        $notificationBuilder->setBody($body)->setSound('default')->setClickAction('MAIN');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['gigs_id' => $gig_id]);
        $dataBuilder->addData(['gigs_name' => $gig_name]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

        // Log::info($downstreamResponse->numberSuccess());
        // Log::info($downstreamResponse->numberFailure());
        // Log::info($downstreamResponse->numberModification());
        //
        // //return Array - you must remove all this tokens in your database
//     Log::info($downstreamResponse->tokensToDelete());
        $tokensToDelete = $downstreamResponse->tokensToDelete();
        FirebaseId::whereIn('instance_id', $tokensToDelete)->delete();

        //
        // //return Array (key : oldToken, value : new token - you must change the token in your database )
        // Log::info($downstreamResponse->tokensToModify());
        $tokensToModify = $downstreamResponse->tokensToModify();
        foreach ($tokensToModify as $tokenToModify) {
            $firebaseId = FirebaseId::where('instance_id', $tokenToModify['key'])->first();
            $firebaseId->instance_id = $tokenToModify['value'];
            $firebaseId->save();
        }
        //
        // //return Array - you should try to resend the message to the tokens in the array
        // Log::info($downstreamResponse->tokensToRetry());

        // return Array (key:token, value:errror) - in production you should remove from your database the tokens
        return 'SUCCESS';
    }
}
