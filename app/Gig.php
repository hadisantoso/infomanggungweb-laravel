<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gig extends Model
{
    public function artists()
    {
        return $this->belongsToMany('App\Artist')->withTimestamps();
    }

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }
}
