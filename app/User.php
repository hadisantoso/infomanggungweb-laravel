<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function artists()
    {
        return $this->belongsToMany('App\Artist')->withTimestamps();
    }

    public function firebaseIds()
    {
        return $this->hasMany('App\FirebaseId');
    }

    public function locations()
    {
        return $this->belongsToMany('App\Location')->withTimestamps();;
    }

    public function gigs()
    {
        return $this->belongsToMany('App\Gig')->withTimestamps();;
    }
}
