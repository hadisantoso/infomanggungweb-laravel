<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gig;
use App\Artist;
use App\User;
use Datatables;
use Alert;
use LaravelFCM\Facades\FCM;
use Illuminate\Support\Facades\Log;
use App\GeneralClasses\Parser;
use App\GeneralClasses\S3Image;
use App\GeneralClasses\SendFcm;
use App\Location;
use App\FirebaseId;


class GigController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin-gig-list');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getdata()
    {
        return Datatables(Gig::all())->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-gig-create-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gig = new Gig;
        if (!empty($request->photo_cover)) {
            $file = $request->photo_cover;
            $photo_url = S3Image::save($file, 'cover', null);
            $gig->photo_cover = $photo_url->image;
            $gig->photo_cover_thumbnail = $photo_url->thumbnail;
        }
        $gig->name = $request->name;
        $gig->description = $request->description;
        $gig->event_date = $request->event_date;
        $gig->place = $request->place;
        $gig->latitude = $request->latitude;
        $gig->longitude = $request->longitude;
        $gig->artists = $request->artists;
        $gig->artists_mention_code = $request->artists_mention_code;
        if($request->is_gigs_slider == "on"){
          $gig->is_gigs_slider = '1';
        }else{
          $gig->is_gigs_slider = '0';
        }

        if($request->is_published == "on"){
          $gig->is_published = '1';
        }else{
          $gig->is_published = '0';
        }
        $gig->save();
        $artist_uid = Parser::getParserString($request->artists_uid, ',');
        for ($i = 0; $i < sizeof($artist_uid); $i++) {
            $artist = Artist::find($artist_uid[$i]);
            $gig->artists()->save($artist);
        }

        //getuser from artist and send FCM

        if ($request->all_check == "on") {
            $tokens = FirebaseId::all()->pluck('instance_id');
            $tokens = $tokens->toArray();
            $start = 0;
            while(true){
                $tmp_tokens = array_slice($tokens,$start,500);
                if(empty($tmp_tokens)){
                  break;
                }
                SendFcm::send($tmp_tokens, 'Check out new gigs', $gig->id, $gig->name);
                $start = $start + 500;
            }
        } elseif ($request->follower_check == "on") {
            for ($i = 0; $i < sizeof($artist_uid); $i++) {
                $artist = Artist::find($artist_uid[$i]);
                $userids = $artist->users()->pluck('id');
                foreach ($userids as $userid) {
                    $usertokens = User::find($userid)->firebaseIds()->pluck('instance_id');
                    SendFcm::send($usertokens->toArray(), 'Check out new gigs for ' . $artist->name, $gig->id, $gig->name);
                }
            }
        }

        //insert location
        if (!empty($request->google_place_id)) {
            $location = Location::where('google_place_id', $request->google_place_id)->first();
            if (empty($location)) {
                $location = new Location;
                $location->google_place_id = $request->google_place_id;
                $location->city_name = $request->city_name;
                $location->save();
            }
            $location->gigs()->save($gig);
        }

        Alert::success('Gig has been inserted successfully');
        return view('admin-gig-list');
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return 'test';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gig = Gig::find($id);
        return view('admin-gig-edit-form', ['gig' => $gig]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gig = Gig::find($id);
        if (!empty($request->photo_cover)) {
            $file = $request->photo_cover;
            $old_photo = new \stdClass();
            $old_photo->image = $gig->photo_cover;
            $old_photo->thumbnail = $gig->photo_cover_thumbnail;
            $photo_url = S3Image::save($file, 'cover', $old_photo);
            $gig->photo_cover = $photo_url->image;
            $gig->photo_cover_thumbnail = $photo_url->thumbnail;
        }
        $gig->name = $request->name;
        $gig->description = $request->description;
        $gig->event_date = $request->event_date;
        $gig->place = $request->place;
        $gig->latitude = $request->latitude;
        $gig->longitude = $request->longitude;
        $gig->artists = $request->artists;
        $gig->artists_mention_code = $request->artists_mention_code;
        if($request->is_gigs_slider == "on"){
          $gig->is_gigs_slider = '1';
        }else{
          $gig->is_gigs_slider = '0';
        }

        if($request->is_published == "on"){
          $gig->is_published = '1';
        }else{
          $gig->is_published = '0';
        }
        $gig->save();
        $gig->artists()->detach();
        $artist_uid = Parser::getParserString($request->artists_uid, ',');
        for ($i = 0; $i < sizeof($artist_uid); $i++) {
            $artist = Artist::find($artist_uid[$i]);
            $gig->artists()->save($artist);
        }

        //insert location
        if (!empty($request->google_place_id)) {
            $location = Location::where('google_place_id', $request->google_place_id)->first();
            if (empty($location)) {
                $location = new Location;
                $location->google_place_id = $request->google_place_id;
                $location->city_name = $request->city_name;
                $location->save();
            }
            $location->gigs()->save($gig);
        }
        Alert::success('Gig has been updated successfully');
        return view('admin-gig-list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gig = Gig::findOrFail($id);
        $old_photo = Parser::getFileName($gig->photo_cover);
        S3Image::delete($old_photo);
        $old_photo = Parser::getFileName($gig->photo_cover_thumbnail);
        S3Image::delete($old_photo);
        $gig->delete();
        return 'SUCCESS';
    }

}
