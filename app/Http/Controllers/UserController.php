<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Alert;


class UserController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth:admin');
  }

  public function index()
  {
      return view('admin-user-list');
  }

  public function getData(){
    return Datatables(User::all())->toJson();
  }

  public function edit($id)
  {
      $user = User::find($id);
      return view('admin-user-edit-form', ['user' => $user]);
  }

  public function update(Request $request, $id)
  {
    $user = User::find($id);
    $user->name = $request->name;
    $user->save();
    Alert::success('User has been updated successfully');
    return view('admin-user-list');
  }

  public function destroy($id)
  {
      $user = User::findOrFail($id);
      $user->delete();
      return 'SUCCESS';
  }
}
