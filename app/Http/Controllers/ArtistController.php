<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artist;
use Datatables;
use Alert;
use Illuminate\Support\Facades\Log;
use App\GeneralClasses\Parser;
use App\GeneralClasses\S3Image;

class ArtistController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$artists = Artist::all();
        //return $artists;
        //return view('artist-list',['artists' => $artists]);
        return view('admin-artist-list');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getdata()
    {
        return Datatables(Artist::all())->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-artist-create-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //Storage::disk('s3')->put('images\hello.txt','world','public');
        $artist = new Artist;
        if (!empty($request->photo_profile)) {
            $file = $request->photo_profile;
            $photo_url = S3Image::save($file, 'profile', null);
            $artist->photo_profile = $photo_url->image;
            $artist->photo_profile_thumbnail = $photo_url->thumbnail;
        }
        if (!empty($request->photo_cover)) {
            $file = $request->photo_cover;
            $photo_url = S3Image::save($file, 'cover', null);
            $artist->photo_cover = $photo_url->image;
            $artist->photo_cover_thumbnail = $photo_url->thumbnail;
        }
        $artist->name = $request->name;
        $artist->bio_indo = $request->bioindo;
        $artist->bio_english = $request->bioenglish;
        $artist->email = $request->email;
        $artist->phone = $request->phone;
        $artist->instagram = $request->instagram;
        $artist->twitter = $request->twitter;
        $artist->facebook = $request->facebook;
        $artist->soundcloud = $request->soundcloud;
        $artist->bandcamp = $request->bandcamp;
        $artist->reverbnation = $request->reverbnation;
        $artist->youtube = $request->youtube;
        $artist->spotify = $request->spotify;
        $artist->website = $request->website;
        if($request->is_published == "on"){
          $artist->is_published = '1';
        }else{
          $artist->is_published = '0';
        }
        $artist->save();
        Alert::success('Artist has been inserted successfully');
        return view('admin-artist-list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artist = Artist::find($id);
        return view('admin-artist-edit-form', ['artist' => $artist]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $artist = Artist::find($id);
        if (!empty($request->photo_profile)) {
            $file = $request->photo_profile;
            $old_photo = new \stdClass();
            $old_photo->image = $artist->photo_cover;
            $old_photo->thumbnail = $artist->photo_cover_thumbnail;
            $photo_url = S3Image::save($file, 'profile', $old_photo);
            $artist->photo_profile = $photo_url->image;
            $artist->photo_profile_thumbnail = $photo_url->thumbnail;
        }
        if (!empty($request->photo_cover)) {
            $file = $request->photo_cover;
            $old_photo = new \stdClass();
            $old_photo->image = $artist->photo_cover;
            $old_photo->thumbnail = $artist->photo_cover_thumbnail;
            $photo_url = S3Image::save($file, 'cover', $old_photo);
            $artist->photo_cover = $photo_url->image;
            $artist->photo_cover_thumbnail = $photo_url->thumbnail;
        }
        $artist->name = $request->name;
        $artist->bio_indo = $request->bioindo;
        $artist->bio_english = $request->bioenglish;
        $artist->email = $request->email;
        $artist->phone = $request->phone;
        $artist->instagram = $request->instagram;
        $artist->twitter = $request->twitter;
        $artist->facebook = $request->facebook;
        $artist->soundcloud = $request->soundcloud;
        $artist->bandcamp = $request->bandcamp;
        $artist->reverbnation = $request->reverbnation;
        $artist->youtube = $request->youtube;
        $artist->spotify = $request->spotify;
        $artist->website = $request->website;
        if($request->is_published == "on"){
          $artist->is_published = '1';
        }else{
          $artist->is_published = '0';
        }
        $artist->save();
        Alert::success('Artist has been updated successfully');
        return view('admin-artist-list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $artist = Artist::findOrFail($id);
        $old_photo = Parser::getFileName($artist->photo_cover);
        S3Image::delete($old_photo);
        $old_photo = Parser::getFileName($artist->photo_cover_thumbnail);
        S3Image::delete($old_photo);
        $old_photo = Parser::getFileName($artist->photo_profile);
        S3Image::delete($old_photo);
        $old_photo = Parser::getFileName($artist->photo_profile_thumbnail);
        S3Image::delete($old_photo);
        $artist->delete();
        return 'SUCCESS';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        //\DB::connection()->enableQueryLog();
        //Log::info('message : '.$request->term);
        $artists = Artist::where('name', 'LIKE', '%' . $request->term . '%')->get();
        // $queries = \DB::getQueryLog();
        // Log::info($queries);
        // $obj1 = new \stdClass;
        // $obj1->value="test";
        // $obj1->label="test1";
        //$cars = array($obj1);
        $arrayArtist = array();
        foreach ($artists as $artist) {
            $data = new \stdClass;
            $data->value = $artist->name;
            $data->uid = $artist->id;
            array_push($arrayArtist, $data);
        }
        return $arrayArtist;
    }
}
